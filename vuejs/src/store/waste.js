// On définit les types de mutations possibles
const WASTE_LIST = 'WASTE_LIST'

// les données qu'on souhaite utiliser / modifier
const state = {
  wastes: [],
}

// les getters pour avoir accès à ces données
const getters = {
  wastes: state => state.wastes,
}

// les fonctions pour modifier ces données
const mutations = {
  [WASTE_LIST]: (state, response) => {
    state.wastes = response
  }
}

// les fonctions qui vont déclancher les mutations
const actions = {
  async getWastes({ commit }) {
    try {
      let response = await axios.post('wastes')
      commit(WASTE_LIST, response.data.data)
    } catch (error) {
      console.log(error)
    }
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}