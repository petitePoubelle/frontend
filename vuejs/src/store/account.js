// On définit les types de mutations possibles
const USER_DATA = 'USER_DATA'

// les données qu'on souhaite utiliser / modifier
const state = {
  user: {
    dumps:[],
    trashes:[],
    skills:[],
    registration:{}
  },
}

// les getters pour avoir accès à ces données
const getters = {
  userData: state => state.user,
  userDumps: state => state.user.dumps.sort(function(a, b) {
      a = new Date(a.date)
      b = new Date(b.date)
      return a<b ? -1 : a>b ? 1 : 0
  }),
  monthDumps: (state,getters) => {
    let dumps = []
    if (getters.userDumps !== []){
      let date = new Date() 
      let endDate = new Date(date.setDate(date.getDate()-30))
      for (let dump of getters.userDumps.filter(dump => (new Date(dump.date) >= endDate))) {
        dumps.push(dump)
      }
      dumps.sort(function(a, b) {
        a = new Date(a.date)
        b = new Date(b.date)
        return a<b ? -1 : a>b ? 1 : 0
      })
    }
    return dumps
  },
  firstMonthDumps: (state,getters) => {
    let dumps = []
    if (getters.userDumps !== []){
      let date = new Date(getters.userDumps['0'].date)
      let endDate = new Date(date.setDate(date.getDate()+30))
      for (let dump of getters.userDumps.filter(dump => (new Date(dump.date) <= endDate))) {
        dumps.push(dump)
      }
      dumps.sort(function(a, b) {
          a = new Date(a.date)
          b = new Date(b.date)
          return a<b ? -1 : a>b ? 1 : 0
      })
    }
    return dumps
  },
  nbTrashes: (state, getters) => {
    let nbTrashes = {
      gris: 0,
      tri: 0
    }
    for (let dump of getters.monthDumps) {
      if (getters.userIdTrashes.grise.includes(dump.trash_id)){
        nbTrashes.gris++
      }
      if (getters.userIdTrashes.tri.includes(dump.trash_id)){
        nbTrashes.tri++
      }
    }
    return nbTrashes
  },
  userIdTrashes:(state) => {
    let idTrashes = {
      grise: [],
      tri: []
    }
    for (let trash of state.user.trashes) {
      if (trash.type === 'grise') {
        idTrashes.grise.push(trash.id)
      } else {
        idTrashes.tri.push(trash.id)
      }
    }
    return idTrashes
  },
  userSkills:(state) => {
    let idSkills = {
      vrac: null,
      diy: null,
      bio: null,
      local: null,
      compost: false,
      progressGrey: null,
      progressTri: null 
    }
    for (let skill of state.user.skills) {
      if (skill.category === 'vrac') {
        idSkills.vrac = skill.id
      } else if (skill.category === 'diy'){
        idSkills.diy = skill.id
      } else if (skill.category === 'bio'){
        idSkills.bio = skill.id
      } else if (skill.category === 'local'){
        idSkills.local = skill.id
      } else if (skill.category === 'composter'){
        idSkills.compost = true
      } else if (skill.category === 'progressGrey'){
        idSkills.progressGrey = skill.id
      } else if (skill.category === 'progressTri'){
        idSkills.progressTri = skill.id
      }
    }
    return idSkills
  }
}

// les fonctions pour modifier ces données
const mutations = {
  [USER_DATA]: (state, response) => {
    state.user = response
  }
}

// les fonctions qui vont déclancher les mutations
const actions = {
  async getUserData({ commit }) {
    try {
      let response = await axios.post('me')
      commit(USER_DATA, response.data.data)
    } catch (error) {
      console.log(error)
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}