import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import Home from '@/components/pages/Home'
import Trashes from '@/components/pages/Trashes'
import Skills from '@/components/pages/Skills'
import Dump from '@/components/pages/Dump'
import Stats from '@/components/pages/Stats'
import Tips from '@/components/pages/tips/Tips'
import Profile from '@/components/pages/Profile'
import Search from '@/components/pages/Search'
import Map from '@/components/pages/Map'

Vue.use(VueRouter)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

const ifAdmin = (to, from, next) => {
  if (store.getters.isAdmin && store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

export default new VueRouter({
  routes: [
    { path: '/', name: 'Dump', component: Dump, beforeEnter: ifAuthenticated },
    { path: '/login', name: 'Login', component: Login, beforeEnter: ifNotAuthenticated },
    { path: '/register', name: 'Register', component: Register,  beforeEnter: ifNotAuthenticated },
    { path: '/home', name: 'Home', component: Home, beforeEnter: ifAuthenticated},
    { path: '/trashes', name: 'Trashes', component: Trashes, beforeEnter: ifAuthenticated},
    { path: '/skills', name: 'Skills', component: Skills, beforeEnter: ifAuthenticated},
    { path: '/tips', name: 'Tips', component: Tips, beforeEnter: ifAuthenticated},
    { path: '/profile', name: 'Profile', component: Profile, beforeEnter: ifAuthenticated},
    { path: '/stats', name: 'Stats', component: Stats, beforeEnter: ifAuthenticated},
    { path: '/search', name: 'Search', component: Search},
    { path: '/map', name: 'Map', component: Map},
    { path: '*', redirect: '/' }
  ]
})
