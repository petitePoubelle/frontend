
import Vue from 'vue'
import VueCordova from 'vue-cordova'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import './configAxios'
import './assets/stylus/main.styl'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(VueCordova)
Vue.use(Vuetify,{
  theme: {
    primary: '#5CBBA2',
    secondary: '#E1F2EE'
  }
})

// add cordova.js only if serving the app through file://
if (window.location.protocol === 'file:' || window.location.port === '3000') {
  var cordovaScript = document.createElement('script')
  cordovaScript.setAttribute('type', 'text/javascript')
  cordovaScript.setAttribute('src', 'cordova.js')
  document.body.appendChild(cordovaScript)
}


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  data: function () {
    return {
      cordova: Vue.cordova
    }
  }
})
